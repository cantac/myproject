package ua.khpi.se.your_cantac.homework05;

public class One {
	public int x = 999;

	public One() {
		System.out.println("One::One()");

	}

	public One(int x) {
		System.out.println("One::One(int)");
		this.x = x;
	}

	public One getInstance() {
		return null;
	}
}
