import java.util.Arrays;

public class ArrayEx {

	// Sort ascending
	static void sort_asc(int[] arr) {
		for (int i = 0; i < arr.length; ++i) {
			for (int j = i + 1; j < arr.length; ++j) {
				if (arr[i] > arr[j]) {
					int temp = arr[i];
					arr[i] = arr[j];
					arr[j] = temp;
				}
			}
		}
	}
	
	// Sort descending
	static void sort_dsc(int[] arr) {
		// TODO: Implement sorting in descending order.
		
		for (int i = 0; i < arr.length; ++i) {
			for (int j = i + 1; j < arr.length; ++j) {
				if (arr[i] < arr[j]) {
					int temp = arr[i];
					arr[i] = arr[j];
					arr[j] = temp;
				}
			}
		}
		
		
		
	}

	
	public static void main(String[] args) {
		int [] x = {9, 3, 5, 7, 8, -1};
		System.out.println(Arrays.toString(x));
		sort_asc(x);
		System.out.println(Arrays.toString(x));
		
		sort_dsc(x);
		System.out.println(Arrays.toString(x));
		

	}

}
