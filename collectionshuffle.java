package collectionshuffle;

import java.util.ArrayList;
import java.util.Collections;

public class collectionshuffle {

	public static void main(String[] args) {
		ArrayList<String> mylist = new ArrayList<String>();
		mylist.add("java");
		mylist.add("c++");
		mylist.add("js");
		mylist.add("css");
		mylist.add("html");
		mylist.add("csharp");

		System.out.println("Original List : \n" + mylist);

		Collections.shuffle(mylist);

		System.out.println("\nShuffled List : \n" + mylist);
	}
}
