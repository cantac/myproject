package pencere;

import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JPanel;
import javax.swing.Timer;

public class KarePencere extends JPanel implements ActionListener,
		MouseListener {

	Cycle[] cycles;
	int cycleMevcut;
	int cycleMax = 10;

	int artisMiktari = 2;

	public KarePencere() {
		super();
		cycles = new Cycle[cycleMax];
		addMouseListener(this);
		Timer zaman = new Timer(40, this);
		zaman.start();

	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		for (int i = 0; i < cycleMevcut; i++) {
			cycles[i].ekranCiz(g);
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		for (int i = 0; i < cycleMevcut; i++) {
			cycles[i].genisle(artisMiktari);
		}

		repaint();
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		if (cycleMevcut < cycleMax - 1) {
			Cycle newCycle = new Cycle(e.getX(), e.getY(), 0, 0);
			cycles[cycleMevcut] = newCycle;
			cycleMevcut++;
		}
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub

	}
}
