package pencere;

import java.awt.Graphics;

public class Cycle {
	int x, y;
	int height;
	int width;

	public Cycle(int x, int y, int height, int width) {
		super();
		this.x = x;
		this.y = y;
		this.height = height;
		this.width = width;
	}

	public void ekranCiz(Graphics g) {
		g.drawOval(x, y, width, height);

	}

	public void genisle(int i) {
		x = x - (i / 2);
		y = y - (i / 2);
		height = height + i;
		width = width + i;
	}

}
