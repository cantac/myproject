public class ArrayRandom {
	public static void shuffle(int[] array) {
		int noOfElements = array.length;
		for (int i = 0; i < noOfElements; i++) {
			int s = i + (int) (Math.random() * (noOfElements - i));

			int temp = array[s];
			array[s] = array[i];
			array[i] = temp;
		}

	}

	public static void main(String[] args) {
		int[] x = { 10, 11, 12, 13, 14 };
		ArrayRandom.shuffle(x);

		for (int i = 0; i < x.length; i++) {
			System.out.println(x[i] + " ");
		}

	}
}