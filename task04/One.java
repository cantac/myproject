package ua.khpi.se.cantac.task04;

class One {
	public static double square(int x) {
		// Calculate and return the square of 'x'.

		return Math.pow(x, 10);

	}

	public static int power(int x, int y) {
		// Return the result of 'x' raised to 'y'th power.
		return (int) Math.pow(x, y);
	}

	public static void power(A a, int y) {
		// Raise 'a.x' to the 'y'th power.
		a.x = (int) Math.pow(a.x, y);

	}

	public static void power(int[] array, int y) {
		// Raise each element of an array to the 'y'th power.
		for (int i = 0; i < array.length; i++) {
			array[i] = (int) Math.pow(array[i], y);
		}
	}

}

class A {
	int x;
}