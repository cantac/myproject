package ua.khpi.se.cantac.homework06;

public class Rectangle {
	public double height;
	public double width;

	public Rectangle(double width, double height) {
		super();
		this.height = height;
		this.width = width;
	}

	public double area() {
		return this.width * this.height;
	}

}
