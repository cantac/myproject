package ua.khpi.se.cantac.homework06;

public class Square {
	public double width;
	public double area;

	public double getArea() {
		return area;
	}

	public void setArea(double area) {
		this.area = area;
	}

	public Square(double width) {
		super();
		this.width = width;

	}

	public double area() {
		return this.width * this.width;
	}
	public double width(){
		return Math.sqrt(area());
	}

	public double getWidth() {
		return width;
	}

	public void setWidth(double width) {
		this.width = width;
	}

}
