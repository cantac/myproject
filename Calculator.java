public class Calculator {

	public static int inc(int a) {

		a++;
		// TODO Add one to 'a'

		// TODO return your calculated value
		// instead of 0
		return (a);
	}

	public static int dec(int a) {
		// TODO Subtract one from 'a'
		a--;
		// TODO return your calculated value
		// instead of 0
		return (a);
	}

	public static int sum(int a, int b) {

		// TODO Calculate the sum of 'a' and 'b'

		// TODO return your calculated value
		// instead of 0
		return (a + b);
	}

	public static String check(int a) {
		String result;
		if (a > 0) {
			result = "positive";
		} else {
			result = "negative";
		}
		// TODO Check that 'a' is positive or negative.
		// If 'a' is positive return "positive".
		// If 'a' is negative return "negative".
		// If a is zero return "zero".
		return result;
	}

	public static void main(String[] args) {

		System.out.println(inc(5));
		System.out.println(dec(5));
		System.out.println(sum(7, 8));
		System.out.println(check(10));

	}

}
