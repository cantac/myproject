package ua.khpi.se.cantac.task05;

public class ConstructorEx {
	int[] array;

	ConstructorEx(int length) {

		array = new int[length];

	}

	ConstructorEx(int[] srcArray) {

		array = srcArray;

	}

	ConstructorEx(int[] srcArray, boolean copyArray) {

		if (copyArray) {
			array = new int[srcArray.length];

			for (int i = 0; i < srcArray.length; i++) {

				array[i] = srcArray[i];

			}
		} else {
			array = srcArray;
		}
	}
}
