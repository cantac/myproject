package ua.khpi.se.cantac.task08;

public class Main {
	public static void main(String[] args) {
		Circle c;
		Rectangle r;
		Square s;
		Object[] shapes = { c = new Circle(10), r = new Rectangle(20, 30),
				s = new Square(40) };

		ShapeUtils.printShapes(shapes);

		shapes[0] = c.getArea();
		shapes[1] = r.getArea();
		shapes[2] = s.getArea();
		Double sum = ShapeUtils.sumAreas(shapes);
		System.out.format("%.2f", sum);

	}

}
