package ua.khpi.se.cantac.task08;

public class ShapeUtils {
	public static void printShapes(Object[] shapes) {
		for (Object s : shapes) {
			System.out.println(s);
		}
	}

	public static double sumAreas(Object[] shapes) {
		double sum = 0;
		int a = 0;
		for (Object s : shapes) {
			// sum = sum + s.getArea(); // Illegal! Object does not have getArea
			sum = sum + (double) shapes[a++];
		}
		return (sum);
	}

}
