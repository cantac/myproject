package ua.khpi.se.cantac.task06;

public class CircleTest {
	public static void main(String[] args) {

		Circle[] circles = new Circle[100];
		for (int i = 0; i < circles.length; i++) {
			double radius = 10 * Math.random();
			circles[i] = new Circle(radius);

			System.out.println("Circle " + i);
			System.out.println("Generated radius: " + radius);
			System.out.println("Stored radius:    " + circles[i].radius);
			System.out.println("Cycle area:    " + circles[i].area());
		}

	}
}
