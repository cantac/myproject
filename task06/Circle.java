package ua.khpi.se.cantac.task06;

public class Circle {
	public double radius;
	public double pi = Math.PI;

	public Circle(double radius) {
		super();
		this.radius = radius;
	}

	public double area() {
		return radius * radius * pi;
	}

}
