package ua.khpi.se.cantac.task03;

public class LoopEx {
	public static void main(String[] args) {
		printNums1(3);
		printNums2(9);
		printNums3(6);
	}

	public static void printNums1(int upperLimit) {

		for (int i = 0; i <= upperLimit; i++) {
			System.out.println(i);
		}
		// TODO: Print out the numbers from 0 up to and including upperLimit.
	}

	public static void printNums2(int upperLimit) {
		for (int i = 0; i <= upperLimit; i = i + 2) {
			System.out.println(i);
		}
	}

	public static void printNums3(int upperLimit) {
		for (int i = 0; i < upperLimit; upperLimit--) {

			System.out.println(upperLimit);
		}
	}

}
